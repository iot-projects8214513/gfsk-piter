# IOT Project - GFSK Module by Piotr Zieliński

This repository contains project of device capable of sending environlmental data (temperature and humidity) over GFSK modulated ??Hz frequency to "HUB".

**LIST OF CONTENT:**

[[_TOC_]]

## Schematic

![Schematic](./IOT_schematic.drawio.png "Schematic")

## 3D printed holder for STM32WL55

Own PCB holder was designed for STM32WL55 using Fusion360. All files can be found in [this directory](./NUCLEO-WL55JC/3D%20printed%20holder%20base/).

4 M2.5 screws (al least 8mm long) and 4 M2.5 inserts are required in order to mount SMT32WL55.

![3d printed holder assembly](./NUCLEO-WL55JC/3D%20printed%20holder%20base/View_Izo.png "3d printed holder assembly")

![3d printed holder](./NUCLEO-WL55JC/3D%20printed%20holder%20base/View_Izo2.png "3d printed holder")

![3d printed holder](./NUCLEO-WL55JC/3D%20printed%20holder%20base/3d_printed_holder.jpg)

![3d printed holder](./NUCLEO-WL55JC/3D%20printed%20holder%20base/3d_printed_holder2.jpg)

## Useful resourcess

* [MOOC - STM32CubeIDE dual core project on STM32WL55 (YouTube playlist)](https://www.youtube.com/playlist?list=PLnMKNibPkDnFPqEmJAwEjsSS8MOQZHRNF)

* [STM32CubeIDE Advanced Debug Features (YT playlist0}](https://www.youtube.com/playlist?list=PLnMKNibPkDnEDEsV7IBXNvg7oNn3MfRd6)

* [MOOC - STM32WL workshop (YT playlist)](https://www.youtube.com/playlist?list=PLnMKNibPkDnGnO6StrHpGdr0cnPh1nw05)

* [STM32WL Series](https://www.st.com/en/microcontrollers-microprocessors/stm32wl-series.html)

* [STM32WL5x](https://www.st.com/en/microcontrollers-microprocessors/stm32wl5x.html)

* [NUCLEO-WL55JC](https://www.st.com/content/st_com/en/products/evaluation-tools/product-evaluation-tools/mcu-mpu-eval-tools/stm32-mcu-mpu-eval-tools/stm32-nucleo-boards/nucleo-wl55jc.html)

* [STM32WL55 IPCC in practice (YT playlist)](https://youtube.com/playlist?list=PLGvhk5cqyfCYHOMd_FVEjlVPWYwh0TDSU)

* [Example of ping pong comunication on 2 NUCLEO-WL55JC2](https://forum.digikey.com/t/using-the-low-level-sub-ghz-radio-driver-for-the-stm32wl-series/18253)

## Author

[**Piotr Zieliński**](https://gitlab.com/piter_t_ziel "@piter_t_ziel") 237457
